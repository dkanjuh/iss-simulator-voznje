using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VehiclePhysics;

public class OppositeDirection3 : MonoBehaviour
{
    public float speed;

    public Rigidbody rb;

    private Quaternion startRotation;

    private bool movementAllowed = true;

    public VPStandardInput vehicleInput;

    /// <summary>
    /// Start position of the car.
    /// </summary>
    /// 

    private Vector3 start = new Vector3(-669.9f, 16.94f, 347.3f);

    /// <summary>
    /// End position of the car.
    /// </summary>
    private Vector3 end = new Vector3(-625.9f, 17.3f, 321.9f);


    // Start is called before the first frame update
    void Start()
    {
        //transform.position.Set(start.x, start.y, start.z);

        // Set the start rotation.
        startRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        // When the car goes beyond the x postion.
        if (transform.position.x >= end.x)
        {
            rb.velocity = new Vector3(0, 0, 0);
            rb.angularVelocity = new Vector3(0, 0, 0);
            transform.SetPositionAndRotation(start, startRotation);
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Keypad2) && movementAllowed)
            rb.AddForce(transform.forward * speed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "VPP Sport Coupe")
        {
            movementAllowed = false;
            vehicleInput.disableThrottleInput = true;
            vehicleInput.disableSteerInput = true;
            vehicleInput.disableBrakeInput = true;
            vehicleInput.disableClutchInput = true;
        }
    }
}
