using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VehiclePhysics;

public class OppositeDirectionDrive : MonoBehaviour
{
    public float speed;

    public Rigidbody rb;

    private Quaternion startRotation;

    private bool movementAllowed = true;

    public VPStandardInput vehicleInput;

    /// <summary>
    /// Start position of the car.
    /// </summary>
    /// 

    private Vector3 start = new Vector3(-400.5f, 24f, 297.8f);

    /// <summary>
    /// End position of the car.
    /// </summary>
    private Vector3 end = new Vector3(-340.3f, 24f, 263.1f);


    // Start is called before the first frame update
    void Start()
    {
        //transform.position.Set(start.x, start.y, start.z);

        // Set the start rotation.
        startRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        // When the car goes beyond the x postion.
        if (transform.position.x >= end.x)
        {
            rb.velocity = new Vector3(0, 0, 0);
            rb.angularVelocity = new Vector3(0, 0, 0);
            transform.SetPositionAndRotation(start, startRotation);
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.L) && movementAllowed)
            rb.AddForce(transform.forward * speed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "VPP Sport Coupe")
        {
            movementAllowed = false;
            vehicleInput.disableThrottleInput = true;
            vehicleInput.disableSteerInput = true;
            vehicleInput.disableBrakeInput = true;
            vehicleInput.disableClutchInput = true;
        }
    }
}
