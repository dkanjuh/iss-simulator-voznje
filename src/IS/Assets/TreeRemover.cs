using UnityEngine;

public class TreeRemover : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();
        foreach (GameObject go in allObjects)
            if (go.activeInHierarchy && (go.name.Contains("tree")))
            {
                Destroy(go);
            }
    }
}
