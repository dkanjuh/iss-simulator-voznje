using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using VehiclePhysics;

public class DriveScript : MonoBehaviour
{
    public float speed;

    public Rigidbody rb;

    private Quaternion startRotation;

    private bool movementAllowed = true;

    public VPStandardInput vehicleInput;

    /// <summary>
    /// Start position of the car.
    /// </summary>
    /// 

    private Vector3 start = new Vector3(-111f, 23.18f, 187f);

    /// <summary>
    /// End position of the car.
    /// </summary>
    private Vector3 end = new Vector3(-155.5f, 22.5f, 97.4f);


    // Start is called before the first frame update
    void Start()
    {
        //transform.position.Set(start.x, start.y, start.z);

        // Set the start rotation.
        startRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        // When the car goes beyond the x postion.
        if (transform.position.x <= end.x)
        {
            rb.velocity = new Vector3(0, 0, 0);
            rb.angularVelocity = new Vector3(0, 0, 0);
            transform.SetPositionAndRotation(start, startRotation);
        }       
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.O) && movementAllowed)
            rb.AddForce(transform.forward * speed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "VPP Sport Coupe")
        {
            movementAllowed = false;
            vehicleInput.disableThrottleInput = true;
            vehicleInput.disableSteerInput = true;
            vehicleInput.disableBrakeInput = true;
            vehicleInput.disableClutchInput = true;
        }
    }
}
