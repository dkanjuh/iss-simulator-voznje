using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VehiclePhysics;

public class FromTheRight2 : MonoBehaviour
{
    public float speed;

    public Rigidbody rb;

    private Quaternion startRotation;

    private bool movementAllowed = true;

    public VPStandardInput vehicleInput;

    /// <summary>
    /// Start position of the car.
    /// </summary>
    /// 

    private Vector3 start = new Vector3(-657.7f, 17.75f, 250.2f);

    /// <summary>
    /// End position of the car.
    /// </summary>
    private Vector3 end = new Vector3(-726.6f, 17.75f, 286.2f);


    // Start is called before the first frame update
    void Start()
    {
        //transform.position.Set(start.x, start.y, start.z);

        // Set the start rotation.
        startRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        // When the car goes beyond the x postion.
        if (transform.position.x <= end.x)
        {
            rb.velocity = new Vector3(0, 0, 0);
            rb.angularVelocity = new Vector3(0, 0, 0);
            transform.SetPositionAndRotation(start, startRotation);
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Keypad3) && movementAllowed)
            rb.AddForce(transform.forward * speed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "VPP Sport Coupe")
        {
            movementAllowed = false;
            vehicleInput.disableThrottleInput = true;
            vehicleInput.disableSteerInput = true;
            vehicleInput.disableBrakeInput = true;
            vehicleInput.disableClutchInput = true;
        }
    }
}
